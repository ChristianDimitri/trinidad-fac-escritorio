﻿Imports System.Data.SqlClient
Imports System.Drawing.Drawing2D
Imports System.Text
Imports softvFacturacion.BAL
Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engin
Imports CrystalDecisions.Shared

Public Class FrmCambioTarjetaSol

    Dim Contratonetcambio As Integer

    Private Sub FrmCambioTarjetaSol_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)

        MUESTRATipoCaja()
        MUESTRATarjetasdelcliente()

    End Sub

    Private Sub MUESTRATipoCaja()
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC MUESTRATipoCaja ")

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            cbTipoCaja.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MUESTRATarjetasdelcliente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
        dgvCajascliente.DataSource = BaseII.ConsultaDT("MUESTRADIGITALDELCLITarjeta")
    End Sub


    Private Sub dgvCajascliente_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCajascliente.CellContentClick

    End Sub

    Private Sub dgvCajascliente_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCajascliente.CurrentCellChanged
        If dgvCajascliente.RowCount > 0 Then
            'tbxCajacabiar.Text = CStr(Me.dgvCajascliente.SelectedCells(1).Value) 'dgvCajascliente.SelectedRows(0).Cells("MACCABLEMODEM").Value.ToString()
            Dim row As DataGridViewRow = dgvCajascliente.CurrentRow
            tbxCajacabiar.Text = CStr(row.Cells("MACCABLEMODEM").Value)
            Contratonetcambio = CStr(row.Cells("CONTRATONET").Value)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If cbTipoCaja.SelectedValue = 0 Then
        '    MsgBox("Seleccione el nuevo tipo de caja por favor", MsgBoxStyle.Information)
        '    Exit Sub
        'Else
        If tbxCajacabiar.Text = "" Then
            MsgBox("Seleccione la Tarjeta a Cambiar", MsgBoxStyle.Information)
            Exit Sub
        End If

        NUECambioTarjetaSol()
        GloBndExt = True
        GloClv_Txt = "CTSOL"
        Me.Close()

    End Sub

    Private Sub NUECambioTarjetaSol()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, GloContrato)
        BaseII.CreateMyParameter("@CONTRATONET", SqlDbType.Int, Contratonetcambio)
        BaseII.CreateMyParameter("@MACANT", SqlDbType.VarChar, tbxCajacabiar.Text, 50)
        BaseII.Inserta("NUECambioTarjetaSol")
    End Sub
End Class