﻿Imports System.Windows.Forms
Imports System.Text
Imports System.Data.SqlClient


Public Class FrmDevolucionAparatosCliente

    Private Sub MUESTRADevolucionAparatosCliente(ByVal Clv_Session As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        dgvDevolucion.DataSource = BaseII.ConsultaDT("MUESTRADevolucionAparatosCliente")
    End Sub

    Private Sub ACTUALIZADevolucionAparatosCliente(ByVal Clv_Session As Integer, ByVal Clv_Cablemodem As Integer, ByVal SeRecogioAparato As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@CLV_CABLEMODEM", SqlDbType.Int, Clv_Cablemodem)
        BaseII.CreateMyParameter("@SERECOGIOAPARATO", SqlDbType.Bit, SeRecogioAparato)
        BaseII.Inserta("ACTUALIZADevolucionAparatosCliente")
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        Dim x As Integer
        For x = 0 To dgvDevolucion.RowCount - 1
            ACTUALIZADevolucionAparatosCliente(eClv_Session, dgvDevolucion.Item(0, x).Value, dgvDevolucion.Item(3, x).Value)
        Next
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmDevolucionAparatosCliente_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        MUESTRADevolucionAparatosCliente(eClv_Session)
    End Sub
End Class