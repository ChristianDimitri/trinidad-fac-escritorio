﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromoContrata
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PnlPromoContrata = New System.Windows.Forms.Panel()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.PnlPromoContrata.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlPromoContrata
        '
        Me.PnlPromoContrata.Controls.Add(Me.Button16)
        Me.PnlPromoContrata.Controls.Add(Me.Button14)
        Me.PnlPromoContrata.Controls.Add(Me.Button15)
        Me.PnlPromoContrata.Controls.Add(Me.Label28)
        Me.PnlPromoContrata.Location = New System.Drawing.Point(6, 7)
        Me.PnlPromoContrata.Name = "PnlPromoContrata"
        Me.PnlPromoContrata.Size = New System.Drawing.Size(525, 221)
        Me.PnlPromoContrata.TabIndex = 520
        '
        'Button16
        '
        Me.Button16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button16.Location = New System.Drawing.Point(206, 173)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(119, 37)
        Me.Button16.TabIndex = 6
        Me.Button16.Text = "Ninguno"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button14.Location = New System.Drawing.Point(321, 92)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(139, 64)
        Me.Button14.TabIndex = 5
        Me.Button14.Text = "Mensualidad sin Contratación"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button15.Location = New System.Drawing.Point(74, 92)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(139, 64)
        Me.Button15.TabIndex = 4
        Me.Button15.Text = "Contratación Bs. 100"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(45, 16)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(436, 87)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Seleccione el tipo de contratación que prefiera el cliente"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmPromoContrata
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(541, 233)
        Me.Controls.Add(Me.PnlPromoContrata)
        Me.Name = "FrmPromoContrata"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promo Contratación"
        Me.PnlPromoContrata.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PnlPromoContrata As System.Windows.Forms.Panel
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label28 As System.Windows.Forms.Label
End Class
