﻿Public Class FrmPromoContrata

    Private Sub Button15_Click(sender As System.Object, e As System.EventArgs) Handles Button15.Click
        MODIFICAContratacion(gloClv_Session, 1)
        Me.Close()
    End Sub

    Private Sub Button14_Click(sender As System.Object, e As System.EventArgs) Handles Button14.Click
        MODIFICAContratacion(gloClv_Session, 2)
        Me.Close()
    End Sub

    Private Sub Button16_Click(sender As System.Object, e As System.EventArgs) Handles Button16.Click
        Me.Close()
    End Sub

    Private Sub MODIFICAContratacion(ByVal Clv_Session As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.Inserta("MODIFICAContratacion")
    End Sub
End Class