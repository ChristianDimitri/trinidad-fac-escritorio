﻿Public Class FrmMuestraStbContratados

    Private Sub FrmMuestraStbContratados_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me)

        'If DataGridView1.Rows.Count > 0 Then
        '    DataGridView1.Rows.Clear()
        'End If
        'If DataGridView2.Rows.Count > 0 Then
        '    DataGridView2.Rows.Clear()
        '    '    End If

        DameIndicadoresContrataDig()

        'If DataGridView1.Enabled = True Then
        DameStbPrincipalContratado()
        'End If

        'If DataGridView2.Enabled = True Then
        DameStbAdicionalContratado()
        'End If
    End Sub

    Private Sub DameIndicadoresContrataDig()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@StbPrincipal", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@PagoContrataPrin", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@NumPagosPrin", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@StbAdicional", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@PagoContrataAdic", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@NumPagosAdic", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Usp_DameIndicadoresContrataDig")
        DataGridView1.Enabled = BaseII.dicoPar("@StbPrincipal").ToString
        DataGridView2.Enabled = BaseII.dicoPar("@StbAdicional").ToString
    End Sub

    Private Sub DameStbPrincipalContratado()
        '@Clv_Session bigint,@Clv_Detalle bigint,@Contrato bigint,@Clv_Usuario VARCHAR(15)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, gloClv_Session)
        DataGridView1.DataSource = BaseII.ConsultaDT("usp_DameStbPrincipalContratado")
        'If DGdV.RowCount = 0 Then Exit Sub

    End Sub

    Private Sub DameStbAdicionalContratado()
        '@Clv_Session bigint,@Clv_Detalle bigint,@Contrato bigint,@Clv_Usuario VARCHAR(15)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, gloClv_Session)
        DataGridView2.DataSource = BaseII.ConsultaDT("usp_DameStbAdicionalContratado")
        'If DGdV.RowCount = 0 Then Exit Sub

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        GrabaDetallegrid()
        Me.Close()
    End Sub

    Private Sub GrabaDetallegrid()
        Try

            Dim clv_detalle As Integer = 0
            Dim clv_unicanet As Integer = 0
            Dim Fila As Integer = 0
            For Fila = 0 To DataGridView1.RowCount - 1
                clv_detalle = 0
                clv_unicanet = 0
                clv_detalle = DataGridView1.Item(3, Fila).Value
                clv_unicanet = DataGridView1.Item(4, Fila).Value
                If DataGridView1.Item(0, Fila).Value = 1 Then
                    usp_GuardaStbPrincipalContrataPagos(gloClv_Session, clv_detalle, clv_unicanet)
                End If
            Next

            Dim clv_detalleA As Integer = 0
            Dim clv_unicanetA As Integer = 0
            Dim FilaA As Integer = 0
            For FilaA = 0 To DataGridView2.RowCount - 1
                clv_detalleA = 0
                clv_unicanetA = 0
                clv_detalleA = DataGridView2.Item(3, FilaA).Value
                clv_unicanetA = DataGridView2.Item(4, FilaA).Value
                If DataGridView2.Item(0, FilaA).Value = 1 Then
                    usp_GuardaStbAdicionalContrataPagos(gloClv_Session, clv_detalleA, clv_unicanetA)
                End If
            Next

            MsgBox("Se Grabo Correctamente", MsgBoxStyle.Information, "Información")
            YaSeleccioneStbContrato = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub usp_GuardaStbPrincipalContrataPagos(Clv_session As Integer, clv_detalle As Integer, clv_unicanet As Integer)
        '@Id Bigint,@Tipo varchar(50),@Recibi bit,@Clv_Session bigint,@Clv_Detalle bigint
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, Clv_session)
            BaseII.CreateMyParameter("@clv_detalle", SqlDbType.Int, clv_detalle)
            BaseII.CreateMyParameter("@clv_unicanet", SqlDbType.Int, clv_unicanet)
            BaseII.Inserta("usp_GuardaStbPrincipalContrataPagos")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub usp_GuardaStbAdicionalContrataPagos(Clv_session As Integer, clv_detalle As Integer, clv_unicanet As Integer)
        '@Id Bigint,@Tipo varchar(50),@Recibi bit,@Clv_Session bigint,@Clv_Detalle bigint
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, Clv_session)
            BaseII.CreateMyParameter("@clv_detalle", SqlDbType.Int, clv_detalle)
            BaseII.CreateMyParameter("@clv_unicanet", SqlDbType.Int, clv_unicanet)
            BaseII.Inserta("usp_GuardaStbAdicionalContrataPagos")
        Catch ex As Exception

        End Try
    End Sub

End Class