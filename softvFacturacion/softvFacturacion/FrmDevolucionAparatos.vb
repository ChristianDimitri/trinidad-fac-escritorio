﻿
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmDevolucionAparatos

    Private Sub SP_GrabaDevolucionAparatos(oID As Long, oTipo As String, oRecibi As Integer, oClv_Session As Long, oClv_Detalle As Long)
        '@Id Bigint,@Tipo varchar(50),@Recibi bit,@Clv_Session bigint,@Clv_Detalle bigint
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Id", SqlDbType.BigInt, oID)
            BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, oTipo, 50)
            BaseII.CreateMyParameter("@Recibi", SqlDbType.Bit, oRecibi)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, oClv_Session)
            BaseII.CreateMyParameter("@Clv_Detalle", SqlDbType.BigInt, oClv_Detalle)
            BaseII.Inserta("SP_GrabaDevolucionAparatos")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GrabaDetallegrid(oClv_Session As Long, oClv_Detalle As Long)
        Try
            Dim oId As Long = 0
            Dim oTipo As String = ""
            Dim Fila As Integer = 0
            For Fila = 0 To DGdV.RowCount - 1
                oId = 0
                oTipo = ""
                oId = DGdV.Item(1, Fila).Value
                oTipo = DGdV.Item(4, Fila).Value
                If DGdV.Item(3, Fila).Value = 1 Or DGdV.Item(3, Fila).Value = True Then
                    SP_GrabaDevolucionAparatos(oId, oTipo, 1, oClv_Session, oClv_Detalle)
                Else
                    SP_GrabaDevolucionAparatos(oId, oTipo, 0, oClv_Session, oClv_Detalle)
                End If
            Next
            MsgBox("Se Grabo Correctamente", MsgBoxStyle.Information, "Información")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Detalle(oClv_Session As Long, oClv_Detallle As Long, oContrato As Long)
        '@Clv_Session bigint,@Clv_Detalle bigint,@Contrato bigint,@Clv_Usuario VARCHAR(15)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, oClv_Session)
        BaseII.CreateMyParameter("@Clv_Detalle", SqlDbType.Int, oClv_Detallle)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, oContrato)
        BaseII.CreateMyParameter("@CancelarTodo", SqlDbType.Bit, BndCancelarTodo)
        DGdV.DataSource = BaseII.ConsultaDT("SP_MuestraAparatosCobroAdeudo")
        'If DGdV.RowCount = 0 Then Exit Sub

    End Sub

    Private Sub BnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BnAceptar.Click
        GrabaDetallegrid(FrmFAC.Clv_Session.Text, FrmFAC.CLV_DETALLETextBox.Text)
        Me.Close()
    End Sub


    Private Sub FrmDevolucionAparatos_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'colorea(Me)
        Panel1.BackColor = FrmFAC.BackColor
        Llena_Detalle(FrmFAC.Clv_Session.Text, FrmFAC.CLV_DETALLETextBox.Text, GloContrato)
    End Sub
End Class