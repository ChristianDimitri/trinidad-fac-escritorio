<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCobroErroneo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCobroErroneo))
        Me.TreeViewIzq = New System.Windows.Forms.TreeView
        Me.TreeViewDer = New System.Windows.Forms.TreeView
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.CobroBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.TSBProcesar = New System.Windows.Forms.ToolStripButton
        Me.ButtonSalir = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ButtonBusContrato = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.ComboBoxTipoNota = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextBoxContrato = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.TreeViewOrden = New System.Windows.Forms.TreeView
        Me.LabelFactura = New System.Windows.Forms.Label
        Me.LabelContrato = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        CType(Me.CobroBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CobroBindingNavigator.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeViewIzq
        '
        Me.TreeViewIzq.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewIzq.Location = New System.Drawing.Point(18, 132)
        Me.TreeViewIzq.Name = "TreeViewIzq"
        Me.TreeViewIzq.Size = New System.Drawing.Size(368, 276)
        Me.TreeViewIzq.TabIndex = 0
        '
        'TreeViewDer
        '
        Me.TreeViewDer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewDer.Location = New System.Drawing.Point(430, 132)
        Me.TreeViewDer.Name = "TreeViewDer"
        Me.TreeViewDer.Size = New System.Drawing.Size(368, 276)
        Me.TreeViewDer.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(125, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Factura"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 114)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 15)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Servicios"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(427, 114)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 15)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Servicios"
        '
        'CobroBindingNavigator
        '
        Me.CobroBindingNavigator.AddNewItem = Nothing
        Me.CobroBindingNavigator.CountItem = Nothing
        Me.CobroBindingNavigator.DeleteItem = Nothing
        Me.CobroBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSBProcesar})
        Me.CobroBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CobroBindingNavigator.MoveFirstItem = Nothing
        Me.CobroBindingNavigator.MoveLastItem = Nothing
        Me.CobroBindingNavigator.MoveNextItem = Nothing
        Me.CobroBindingNavigator.MovePreviousItem = Nothing
        Me.CobroBindingNavigator.Name = "CobroBindingNavigator"
        Me.CobroBindingNavigator.PositionItem = Nothing
        Me.CobroBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CobroBindingNavigator.Size = New System.Drawing.Size(1008, 25)
        Me.CobroBindingNavigator.TabIndex = 20
        Me.CobroBindingNavigator.TabStop = True
        Me.CobroBindingNavigator.Text = "BindingNavigator1"
        '
        'TSBProcesar
        '
        Me.TSBProcesar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TSBProcesar.Image = CType(resources.GetObject("TSBProcesar.Image"), System.Drawing.Image)
        Me.TSBProcesar.Name = "TSBProcesar"
        Me.TSBProcesar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TSBProcesar.Size = New System.Drawing.Size(101, 22)
        Me.TSBProcesar.Text = "&PROCESAR"
        '
        'ButtonSalir
        '
        Me.ButtonSalir.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(868, 686)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(136, 36)
        Me.ButtonSalir.TabIndex = 21
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ButtonBusContrato)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.ComboBoxTipoNota)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.TextBoxContrato)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(77, 580)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(821, 100)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'ButtonBusContrato
        '
        Me.ButtonBusContrato.Location = New System.Drawing.Point(289, 18)
        Me.ButtonBusContrato.Name = "ButtonBusContrato"
        Me.ButtonBusContrato.Size = New System.Drawing.Size(28, 23)
        Me.ButtonBusContrato.TabIndex = 22
        Me.ButtonBusContrato.Text = "..."
        Me.ButtonBusContrato.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(88, 66)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(89, 15)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Tipo de Nota"
        '
        'ComboBoxTipoNota
        '
        Me.ComboBoxTipoNota.DisplayMember = "Nombre"
        Me.ComboBoxTipoNota.FormattingEnabled = True
        Me.ComboBoxTipoNota.Location = New System.Drawing.Point(183, 58)
        Me.ComboBoxTipoNota.Name = "ComboBoxTipoNota"
        Me.ComboBoxTipoNota.Size = New System.Drawing.Size(317, 23)
        Me.ComboBoxTipoNota.TabIndex = 20
        Me.ComboBoxTipoNota.ValueMember = "Clv"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(116, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 15)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Contrato"
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.Location = New System.Drawing.Point(183, 20)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxContrato.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TreeViewOrden)
        Me.GroupBox2.Controls.Add(Me.LabelFactura)
        Me.GroupBox2.Controls.Add(Me.LabelContrato)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.TreeViewIzq)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TreeViewDer)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(77, 49)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(821, 494)
        Me.GroupBox2.TabIndex = 23
        Me.GroupBox2.TabStop = False
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label9.Location = New System.Drawing.Point(14, 94)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(368, 20)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Antes de Procesar"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label4.Location = New System.Drawing.Point(426, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(368, 20)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Después de Procesar"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(427, 411)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 15)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Órdenes"
        '
        'TreeViewOrden
        '
        Me.TreeViewOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewOrden.Location = New System.Drawing.Point(430, 429)
        Me.TreeViewOrden.Name = "TreeViewOrden"
        Me.TreeViewOrden.Size = New System.Drawing.Size(368, 58)
        Me.TreeViewOrden.TabIndex = 19
        '
        'LabelFactura
        '
        Me.LabelFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFactura.ForeColor = System.Drawing.SystemColors.Highlight
        Me.LabelFactura.Location = New System.Drawing.Point(180, 60)
        Me.LabelFactura.Name = "LabelFactura"
        Me.LabelFactura.Size = New System.Drawing.Size(155, 20)
        Me.LabelFactura.TabIndex = 18
        Me.LabelFactura.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelContrato
        '
        Me.LabelContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelContrato.ForeColor = System.Drawing.SystemColors.Highlight
        Me.LabelContrato.Location = New System.Drawing.Point(180, 27)
        Me.LabelContrato.Name = "LabelContrato"
        Me.LabelContrato.Size = New System.Drawing.Size(155, 20)
        Me.LabelContrato.TabIndex = 17
        Me.LabelContrato.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(119, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 15)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Contrato"
        '
        'BackgroundWorker1
        '
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Red
        Me.Label10.Location = New System.Drawing.Point(73, 557)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(825, 20)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Cliente al que se le generará la Nota de Crédito"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(73, 26)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(825, 20)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Cliente al que se le generó Erronamente el Cobro"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FrmCobroErroneo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.CobroBindingNavigator)
        Me.Name = "FrmCobroErroneo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Corrección de Pagos Equivocados"
        CType(Me.CobroBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CobroBindingNavigator.ResumeLayout(False)
        Me.CobroBindingNavigator.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeViewIzq As System.Windows.Forms.TreeView
    Friend WithEvents TreeViewDer As System.Windows.Forms.TreeView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CobroBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents TSBProcesar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LabelFactura As System.Windows.Forms.Label
    Friend WithEvents LabelContrato As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxTipoNota As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonBusContrato As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TreeViewOrden As System.Windows.Forms.TreeView
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
