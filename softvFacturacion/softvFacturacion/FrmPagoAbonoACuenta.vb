﻿Imports System.Data.SqlClient

Public Class FrmPagoAbonoACuenta
    Dim LocImpTotal As Double = 0
    Dim BND As Integer = 0
    Dim Bandera As Boolean = False
    Dim ImporteBaja As Double

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.TextBox1.Text = ""
        BNDLIMPIAR = True
        GloBndPagAbnCobBaj = False
        Me.Enabled = True
        FrmFAC.Show()
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        ValidaDecimal(e, TextBox1)
    End Sub

    Private Sub FrmPagoAbonoACuenta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'GloBndPagAbnCobBaj = False
        'BNDLIMPIAR = True
    End Sub

    Private Sub FrmPagoAbonoACuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        BND = 0
        Me.TextBox1.Text = ""
        Me.lblImporteTotal.ForeColor = Color.Red
        Me.lblMsjImporte.ForeColor = Color.Black
        Me.lblTitulo.ForeColor = Color.Black
        Me.Label1.ForeColor = Color.Black

        If GloBndDividePagos = True Then
            Me.Label2.Visible = True
            Me.Label4.Visible = True
            Me.Label7.Visible = True
            Me.LBPagEco.Visible = True
            Me.LBPagoMinEco.Visible = True
            Me.LBPagosParEco.Visible = True
            Me.Label2.ForeColor = Color.Black
            Me.Label4.ForeColor = Color.Black
            Me.Label7.ForeColor = Color.Black
            Me.LBPagEco.ForeColor = Color.Black
            Me.LBPagoMinEco.ForeColor = Color.Black
            Me.LBPagosParEco.ForeColor = Color.Black
        End If

        If IsNumeric(GloImporteTotalEstadoDeCuenta) = False Then GloImporteTotalEstadoDeCuenta = 0

        LocImpTotal = CType(GloImporteTotalEstadoDeCuenta, Double)

        Me.lblImporteTotal.Text = CType(GloImporteTotalEstadoDeCuenta, Double)
        Me.lblImporteTotal.Text = FormatCurrency(Me.lblImporteTotal.Text, 2, TriState.True)

        DameIndicadoresEco()

        'If Recon = 1 Then

        '    DameImporteClientesBaja()

        '    LocImpTotal = ImporteBaja

        '    Me.lblImporteTotal.Text = ImporteBaja
        '    Me.lblImporteTotal.Text = FormatCurrency(Me.lblImporteTotal.Text, 2, TriState.True)

        'End If

        Me.LBPagEco.Text = EcoMeses
        Me.LBPagoMinEco.Text = EcoImpMin
        Me.LBPagosParEco.Text = (Me.lblImporteTotal.Text / EcoMeses)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim locImpCap As Double = 0
        If IsNumeric(TextBox1.Text) = True Then locImpCap = TextBox1.Text Else locImpCap = 0


        If GloBndPagAbnCobBaj = True Then
            If locImpCap > LocImpTotal Then
                MsgBox("La cantidad capturada debe ser menor o igual al adeudo..", MsgBoxStyle.Exclamation, "¡Importe NO válido!")
                Me.TextBox1.Focus()
                Exit Sub
            End If
        End If

        If Not IsNumeric(Me.TextBox1.Text) Then
            MsgBox("Debes de colocar un Importe válido.", MsgBoxStyle.Exclamation, "¡Importe NO válido!")
            Me.TextBox1.Focus()
            Exit Sub
        End If

        If GloBndDividePagos = True Then
            If Me.TextBox1.Text < EcoImpMin Then
                MsgBox("El importe es menor al Importe Minimo", MsgBoxStyle.Information, "¡Importe NO válido!")
                Exit Sub
            End If
            If lblImporteTotal.Text = 0 Then
                GloBndDividePagos = False
                MsgBox("¡Si va a saldar el total del adeudo no es necesario dividir el importe!", MsgBoxStyle.Information, "¡Importe NO válido!")
                Exit Sub
            End If
        End If

        Try
            If LocImpTotal >= locImpCap Then
                If locImpCap = LocImpTotal Then
                    BND = 1
                End If
                Boton_Aceptar()

            Else
                MsgBox("El abono a realizar no puede ser mayor al importe total.", MsgBoxStyle.Information, "¡Importe NO válido!")
                Me.TextBox1.Focus()
                Exit Sub
            End If
            Bandera = True
            Me.Enabled = True
            FrmFAC.Show()
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.InnerException.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub Boton_Aceptar()
        If IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Captura el Importe.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        'If BndEco = True Then
        '    InsertaPagoParaAbonoACuenta(gloClv_Session, Me.TextBox1.Text)
        'ElseIf Recon = 1 Then
        '    InsertaPagoParaAbonoACuentaRecon(gloClv_Session, Me.TextBox1.Text)
        'End If
        InsertaPagoParaAbonoACuenta(gloClv_Session, Me.TextBox1.Text)
        GloAbonoACuenta = True
        GloBndPagAbnCobBaj = True

    End Sub

    Private Sub InsertaPagoParaAbonoACuenta(ByVal Clv_Session As Long, ByVal Importe As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPagoParaAbonarACuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Importe
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = GloContrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@CLVSESSIONABONO", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = GloClvSessionAbono
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@BND", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = BND
        comando.Parameters.Add(parametro5)

        'Dim parametro6 As New SqlParameter("@SiAdeudo", SqlDbType.BigInt)
        'parametro6.Direction = ParameterDirection.Input
        'parametro6.Value = SiAdeudo
        'comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "InsertaPagoParaAbonarACuenta"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub InsertaPagoParaAbonoACuentaRecon(ByVal Clv_Session As Long, ByVal Importe As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPagoParaAbonarEconoPak", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Importe
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = GloContrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@CLVSESSIONABONO", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = GloClvSessionAbono
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@BND", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = BND
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "InsertaPagoParaAbonarEconoPak"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub FrmPagoAbonoACuenta_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If Bandera = False Then
            GloBndPagAbnCobBaj = False
            BNDLIMPIAR = True
        End If
        FrmFAC.Show()
    End Sub

    Private Sub DameIndicadoresEco()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Meses", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@ImporteMin", ParameterDirection.Output, SqlDbType.Money)
        BaseII.ProcedimientoOutPut("DameIndicadoresEco")
        EcoMeses = CInt(BaseII.dicoPar("@Meses").ToString)
        EcoImpMin = CDbl(BaseII.dicoPar("@ImporteMin").ToString)
    End Sub

    Private Sub CambioEconoPack()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@Meses", SqlDbType.Int, LBPagEco.Text)
        BaseII.CreateMyParameter("@ImporteM", SqlDbType.Money, LBPagosParEco.Text)
        BaseII.ProcedimientoOutPut("Usp_CambioEconoPack")

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If IsNumeric(TextBox1.Text) = True Then
            'If Recon = 1 Then
            '    Me.lblImporteTotal.Text = ImporteBaja
            'Else
            Me.lblImporteTotal.Text = CType(GloImporteTotalEstadoDeCuenta, Double)
            'End If

            Me.lblImporteTotal.Text = (Me.lblImporteTotal.Text - TextBox1.Text)
            Me.LBPagosParEco.Text = (Me.lblImporteTotal.Text / EcoMeses)

        ElseIf TextBox1.Text = "" Then
            'If Recon = 1 Then
            '    Me.lblImporteTotal.Text = ImporteBaja
            '    Me.LBPagosParEco.Text = (Me.lblImporteTotal.Text / EcoMeses)
            'Else
            Me.lblImporteTotal.Text = CType(GloImporteTotalEstadoDeCuenta, Double)
            Me.LBPagosParEco.Text = (Me.lblImporteTotal.Text / EcoMeses)
            'End If
        End If

    End Sub

    Private Sub DameImporteClientesBaja()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
        BaseII.CreateMyParameter("@Importe", ParameterDirection.Output, SqlDbType.Money)
        BaseII.ProcedimientoOutPut("DameImporteClientesBaja")
        ImporteBaja = CDbl(BaseII.dicoPar("@Importe").ToString)

    End Sub
End Class