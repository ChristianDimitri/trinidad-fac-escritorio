﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioCajaSol
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbTipoCaja = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvCajascliente = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbxCajacabiar = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CONTRATONET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MACCABLEMODEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvCajascliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbTipoCaja
        '
        Me.cbTipoCaja.DisplayMember = "Caja"
        Me.cbTipoCaja.FormattingEnabled = True
        Me.cbTipoCaja.Location = New System.Drawing.Point(21, 64)
        Me.cbTipoCaja.Name = "cbTipoCaja"
        Me.cbTipoCaja.Size = New System.Drawing.Size(230, 21)
        Me.cbTipoCaja.TabIndex = 0
        Me.cbTipoCaja.ValueMember = "NoArticulo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(186, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Seleccione El tipo de caja que desea:"
        '
        'dgvCajascliente
        '
        Me.dgvCajascliente.AllowUserToAddRows = False
        Me.dgvCajascliente.AllowUserToDeleteRows = False
        Me.dgvCajascliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCajascliente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CONTRATONET, Me.MACCABLEMODEM})
        Me.dgvCajascliente.Location = New System.Drawing.Point(38, 161)
        Me.dgvCajascliente.Name = "dgvCajascliente"
        Me.dgvCajascliente.Size = New System.Drawing.Size(296, 150)
        Me.dgvCajascliente.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 145)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Cajas Actuales del cliente:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.tbxCajacabiar)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(525, 221)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seleccione la caja que se va a remplazar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(347, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Caja a reemplazar:"
        '
        'tbxCajacabiar
        '
        Me.tbxCajacabiar.Enabled = False
        Me.tbxCajacabiar.Location = New System.Drawing.Point(350, 129)
        Me.tbxCajacabiar.Name = "tbxCajacabiar"
        Me.tbxCajacabiar.Size = New System.Drawing.Size(149, 20)
        Me.tbxCajacabiar.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(347, 348)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(462, 348)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Salir"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'CONTRATONET
        '
        Me.CONTRATONET.DataPropertyName = "CONTRATONET"
        Me.CONTRATONET.HeaderText = "ContratoNet"
        Me.CONTRATONET.Name = "CONTRATONET"
        Me.CONTRATONET.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.CONTRATONET.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'MACCABLEMODEM
        '
        Me.MACCABLEMODEM.DataPropertyName = "MACCABLEMODEM"
        Me.MACCABLEMODEM.HeaderText = "Mac"
        Me.MACCABLEMODEM.Name = "MACCABLEMODEM"
        Me.MACCABLEMODEM.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MACCABLEMODEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'FrmCambioCajaSol
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(549, 383)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvCajascliente)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbTipoCaja)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmCambioCajaSol"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Caja"
        CType(Me.dgvCajascliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbTipoCaja As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvCajascliente As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbxCajacabiar As System.Windows.Forms.TextBox
    Friend WithEvents CONTRATONET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MACCABLEMODEM As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
