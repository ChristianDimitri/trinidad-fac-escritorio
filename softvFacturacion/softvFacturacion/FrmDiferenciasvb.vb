﻿Public Class FrmDiferenciasvb

    Private Sub FrmDiferenciasvb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        ''LLENAMOS EL COMBO DE LA CAJERA (INICIO)
        uspLlenaComboCajera()
        ''LLENAMOS EL COMBO DE LA CAJERA (FIN)

        ''LLENAMOS EL COMBO DEL USUARIO QUE AUTORIZA (INICIO)
        uspLlenaComboUsuarios()
        ''LLENAMOS EL COMBO DEL USUARIO QUE AUTORIZA (FIN)

        ''LLENAMOS EL DATA GRID CON TODA LA INFORMACIÓN EXISTENTE AL ABRIR LA FORMA (INICIO)
        uspConsultaTblDiferenciasCortesGlobales("TODOS", "TODOS", "N")
        ''LLENAMOS EL DATA GRID CON TODA LA INFORMACIÓN EXISTENTE AL ABRIR LA FORMA (FIN)
    End Sub

    Private Sub uspLlenaComboCajera()
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbCajera.DisplayMember = "nombre"
        Me.cmbCajera.ValueMember = "clvUsuario"
        Me.cmbCajera.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboCajera")
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (FIN)
    End Sub

    Private Sub uspLlenaComboUsuarios()
        '''''LLENAMOS EL COMBO DEL USUARIO QUE AUTORIZA (INICIO)
        Dim DT1 As New DataTable

        ControlEfectivoClass.limpiaParametros()
        ControlEfectivoClass.CreateMyParameter("@op", SqlDbType.Int, 2)
        DT1 = ControlEfectivoClass.ConsultaDT("uspLlenaComboUsuarios")

        Me.cmbAutoriza.DisplayMember = "nombre"
        Me.cmbAutoriza.ValueMember = "clvUsuario"
        Me.cmbAutoriza.DataSource = DT1
        '''''LLENAMOS EL COMBO DEL USUARIO QUE AUTORIZA (FIN)
    End Sub

    Private Sub uspConsultaTblDiferenciasCortesGlobales(ByVal prmCajera As String, ByVal prmAutoriza As String, ByVal prmStatus As String)
        '''''LLENAMOS EL DATAGRID CON LA INFORMACIÓN CORRESPONDIENTE SOLICITADA (INICIO)
        Dim DT2 As New DataTable

        ControlEfectivoClass.limpiaParametros()
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@autoriza", SqlDbType.VarChar, prmAutoriza, 5)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        DT2 = ControlEfectivoClass.ConsultaDT("uspConsultaTblDiferenciasCortesGlobales")

        Me.dgvDiferencias.DataSource = DT2
        Me.dgvDiferencias.Columns(5).DefaultCellStyle.Format = "c"
        '''''LLENAMOS EL DATAGRID CON LA INFORMACIÓN CORRESPONDIENTE SOLICITADA (FIN)
    End Sub

    Private Sub uspModificaTblDiferenciasCortesGlobales(ByVal prmIdDescarga As Long, ByVal prmAutoriza As String, ByVal prmStatus As String)
        ''''EJECUTAMOS EL PROCEDIMIENTO QUE ACTUALIZA LA TABLA DE LAS DIFERENCIAS DE LAS CAJERAS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        ControlEfectivoClass.CreateMyParameter("@idDiferencia", SqlDbType.BigInt, prmIdDescarga)
        ControlEfectivoClass.CreateMyParameter("@autoriza", SqlDbType.VarChar, prmAutoriza, 5)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        ControlEfectivoClass.Inserta("uspModificaTblDiferenciasCortesGlobales")

        ''LLENAMOS EL DATA GRID CON TODA LA INFORMACIÓN EXISTENTE AL ABRIR LA FORMA (INICIO)
        uspConsultaTblDiferenciasCortesGlobales("TODOS", "TODOS", "N")
        ''LLENAMOS EL DATA GRID CON TODA LA INFORMACIÓN EXISTENTE AL ABRIR LA FORMA (FIN)

        If prmStatus = "A" Then
            MsgBox("Registro Saldado Existosamente", MsgBoxStyle.Information)
            Exit Sub
        ElseIf prmStatus = "P" Then
            MsgBox("Registro Cancelado Existosamente", MsgBoxStyle.Information)
            Exit Sub
        End If
        ''''EJECUTAMOS EL PROCEDIMIENTO QUE ACTUALIZA LA TABLA DE LAS DIFERENCIAS DE LAS CAJERAS (FIN)
    End Sub

    Private Sub btnSaldar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaldar.Click
        ''''MADNAMOS MODIFICAR LA TABLA DE LAS DIFERENCIAS Y SALDAMOS LA DIFERENCIA SELECCIONADA (INICIO)
        If Me.dgvDiferencias.RowCount > 0 Then
            If Me.dgvDiferencias.SelectedCells(6).Value.ToString = "SIN SALDAR" Then
                uspModificaTblDiferenciasCortesGlobales(Me.dgvDiferencias.SelectedCells(0).Value, GloUsuario, "A")
            Else
                MsgBox("El Registro Seleccionado ya se encuentra Saldado", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("Seleccione al menos un Registro", MsgBoxStyle.Information)
            Exit Sub
        End If
        ''''MADNAMOS MODIFICAR LA TABLA DE LAS DIFERENCIAS Y SALDAMOS LA DIFERENCIA SELECCIONADA (FIN)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ''''MADNAMOS CANCELAR LA TABLA DE LAS DIFERENCIAS Y SALDAMOS LA DIFERENCIA SELECCIONADA (INICIO)
        If Me.dgvDiferencias.RowCount > 0 Then
            If Me.dgvDiferencias.SelectedCells(6).Value.ToString = "SALDADA" Then
                uspModificaTblDiferenciasCortesGlobales(Me.dgvDiferencias.SelectedCells(0).Value, GloUsuario, "P")
            Else
                MsgBox("El Registro Seleccionado se encuentra Sin Saldar", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("Seleccione al menos un Registro", MsgBoxStyle.Information)
            Exit Sub
        End If
        ''''MADNAMOS CANCELAR LA TABLA DE LAS DIFERENCIAS Y SALDAMOS LA DIFERENCIA SELECCIONADA (FIN)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub rbSinSaldar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSinSaldar.CheckedChanged
        llenaGrid()
    End Sub

    Private Sub rbSaldadas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSaldadas.CheckedChanged
        llenaGrid()
    End Sub

    Private Sub cmbCajera_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCajera.SelectedIndexChanged
        llenaGrid()
    End Sub

    Private Sub cmbAutoriza_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAutoriza.SelectedIndexChanged
        llenaGrid()
    End Sub

    Private Sub llenaGrid()
        Dim statusDiferencia As String = "N"
        If Me.rbSinSaldar.Checked Then
            statusDiferencia = "P"
        ElseIf Me.rbSaldadas.Checked Then
            statusDiferencia = "A"
        End If
        Try
            uspConsultaTblDiferenciasCortesGlobales(Me.cmbCajera.SelectedValue, Me.cmbAutoriza.SelectedValue, statusDiferencia)
        Catch ex As Exception

        End Try
    End Sub
End Class