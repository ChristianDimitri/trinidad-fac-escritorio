﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepPagosDifFac
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbPeriodo2 = New System.Windows.Forms.CheckBox
        Me.cbPeriodo1 = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cbMes = New System.Windows.Forms.ComboBox
        Me.cbAnio = New System.Windows.Forms.ComboBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cbPago2 = New System.Windows.Forms.CheckBox
        Me.cbPago1 = New System.Windows.Forms.CheckBox
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbPeriodo2)
        Me.GroupBox1.Controls.Add(Me.cbPeriodo1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 72)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Periodos"
        '
        'cbPeriodo2
        '
        Me.cbPeriodo2.AutoSize = True
        Me.cbPeriodo2.Location = New System.Drawing.Point(197, 31)
        Me.cbPeriodo2.Name = "cbPeriodo2"
        Me.cbPeriodo2.Size = New System.Drawing.Size(83, 19)
        Me.cbPeriodo2.TabIndex = 4
        Me.cbPeriodo2.Text = "Segundo"
        Me.cbPeriodo2.UseVisualStyleBackColor = True
        '
        'cbPeriodo1
        '
        Me.cbPeriodo1.AutoSize = True
        Me.cbPeriodo1.Location = New System.Drawing.Point(58, 31)
        Me.cbPeriodo1.Name = "cbPeriodo1"
        Me.cbPeriodo1.Size = New System.Drawing.Size(77, 19)
        Me.cbPeriodo1.TabIndex = 3
        Me.cbPeriodo1.Text = "Primero"
        Me.cbPeriodo1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbMes)
        Me.GroupBox2.Controls.Add(Me.cbAnio)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(312, 72)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fecha de último pago"
        '
        'cbMes
        '
        Me.cbMes.DisplayMember = "Mes"
        Me.cbMes.FormattingEnabled = True
        Me.cbMes.Location = New System.Drawing.Point(27, 29)
        Me.cbMes.Name = "cbMes"
        Me.cbMes.Size = New System.Drawing.Size(121, 23)
        Me.cbMes.TabIndex = 4
        Me.cbMes.ValueMember = "Id"
        '
        'cbAnio
        '
        Me.cbAnio.DisplayMember = "Anio"
        Me.cbAnio.FormattingEnabled = True
        Me.cbAnio.Location = New System.Drawing.Point(168, 29)
        Me.cbAnio.Name = "cbAnio"
        Me.cbAnio.Size = New System.Drawing.Size(121, 23)
        Me.cbAnio.TabIndex = 3
        Me.cbAnio.ValueMember = "Id"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cbPago2)
        Me.GroupBox3.Controls.Add(Me.cbPago1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 168)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(312, 72)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Número de Pagos"
        '
        'cbPago2
        '
        Me.cbPago2.AutoSize = True
        Me.cbPago2.Location = New System.Drawing.Point(197, 30)
        Me.cbPago2.Name = "cbPago2"
        Me.cbPago2.Size = New System.Drawing.Size(71, 19)
        Me.cbPago2.TabIndex = 6
        Me.cbPago2.Text = "Pago 2"
        Me.cbPago2.UseVisualStyleBackColor = True
        '
        'cbPago1
        '
        Me.cbPago1.AutoSize = True
        Me.cbPago1.Location = New System.Drawing.Point(58, 30)
        Me.cbPago1.Name = "cbPago1"
        Me.cbPago1.Size = New System.Drawing.Size(71, 19)
        Me.cbPago1.TabIndex = 5
        Me.cbPago1.Text = "Pago 1"
        Me.cbPago1.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(26, 256)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(180, 256)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'FrmRepPagosDifFac
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(337, 308)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmRepPagosDifFac"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Pagos Diferidos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cbPeriodo1 As System.Windows.Forms.CheckBox
    Friend WithEvents cbPeriodo2 As System.Windows.Forms.CheckBox
    Friend WithEvents cbPago2 As System.Windows.Forms.CheckBox
    Friend WithEvents cbPago1 As System.Windows.Forms.CheckBox
    Friend WithEvents cbMes As System.Windows.Forms.ComboBox
    Friend WithEvents cbAnio As System.Windows.Forms.ComboBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
