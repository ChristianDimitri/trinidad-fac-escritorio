Public Class FrmSelFechaGral

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        rBndTarjetas = False
        Me.Close()
    End Sub

    Private Sub FrmSelFechaGral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        If rBndTarjetas = True Then
            Me.CMBLabel5.Visible = True
            Me.CheckBox1.Visible = True
            Me.CheckBox2.Visible = True
        End If
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        locGlo_Fechaini = Me.DateTimePicker1.Text
        locGlo_Fechafin = Me.DateTimePicker2.Text
        'LiTipo = 4
        Dim opc As Integer = 0
        Dim tarjeta As String
        If rBndTarjetas = True Then
            rBndTarjetas = False
            If Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False Then
                MsgBox("Debe seleccionar al menos un tipo de tarjeta.", MsgBoxStyle.Critical, "ERROR")
                Return
            End If
            If Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = True Then
                opc = 0
                tarjeta = ""
            Else
                opc = 1
                If Me.CheckBox1.Checked = True Then
                    tarjeta = "Tarjeta De Credito"
                Else
                    tarjeta = "Tarjeta Debito"
                End If
            End If
            Dim imprime As FrmImprimir = New FrmImprimir
            imprime.ReporteIngresosTarjetas(locGlo_Fechaini, locGlo_Fechafin, opc, tarjeta)
            imprime.Show()
            Me.Close()
            Return
        End If
        bndreporteconciliacion = True
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub
End Class