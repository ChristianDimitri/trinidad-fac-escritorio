﻿Public Class FrmFiltroEfectivo

    Private Sub FrmFiltroEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbCajera.DisplayMember = "nombre"
        Me.cmbCajera.ValueMember = "clvUsuario"
        Me.cmbCajera.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboCajera")
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (FIN)
    End Sub

    '(@dia INT,@mes INT,@bndDia BIT OUTPUT)
    Private Function uspValidaFechas(ByVal prmMes As Integer, ByVal prmDia As Integer) As Boolean
        '''''VALIDAMOS EN DÍA PARA VER SINO ES MAYOR AL NÚMERO DE DÍAS DEL MES (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@dia", SqlDbType.Int, prmDia)
        ControlEfectivoClass.CreateMyParameter("@mes", SqlDbType.Int, prmMes)
        ControlEfectivoClass.CreateMyParameter("@bndDia", ParameterDirection.Output, SqlDbType.Bit)

        Dim Diccionario As New Dictionary(Of String, Object)

        Diccionario = ControlEfectivoClass.ProcedimientoOutPut("uspValidaFechas")
        uspValidaFechas = Diccionario("@bndDia").ToString()
        '''''VALIDAMOS EN DÍA PARA VER SINO ES MAYOR AL NÚMERO DE DÍAS DEL MES (FIN)
    End Function

    Private Sub uspReporteControlEfectivo(ByVal prmCajera As String, ByVal prmfechaInicial As Date, ByVal prmfechaFinal As Date)
        '''''MANDAMOS EL REPORTE (INICIO)
        ControlEfectivoClass.limpiaParametros() ''LIMPIAMOS LA LISTA DE PARÁMETROS

        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5) ''MANDAMOS LOS PARÁMETROS
        ControlEfectivoClass.CreateMyParameter("@fechaInicial", SqlDbType.DateTime, prmfechaInicial)
        ControlEfectivoClass.CreateMyParameter("@fechaFinal", SqlDbType.DateTime, prmfechaFinal)

        Dim listaTablas As New List(Of String) ''MANDAMOS EL NOMBRE DE LAS TABLAS QUE DEVOLVERÁ EL DATASET DEL REPORTE
        listaTablas.Add("FlujoEfectivo")
        listaTablas.Add("SaldosTotales")

        Dim DataSet As New DataSet ''LLENAMOS EL DATASET QUE LLENARÁ EL REPORTE
        DataSet = ControlEfectivoClass.ConsultaDS("uspReporteControlEfectivo", listaTablas)

        Dim diccioFormulasReporte As New Dictionary(Of String, String) ''LENAMOS EL DICCIONARIO QUE CONTENDRÁ LAS FÓRMULAS QUE REQUIERE EL REPORTE
        diccioFormulasReporte.Add("CIUDAD", LocNomEmpresa)
        diccioFormulasReporte.Add("EMPRESA", GloNomSucursal)
        diccioFormulasReporte.Add("diaInicial", CStr(prmfechaInicial))
        diccioFormulasReporte.Add("diaFinal", CStr(prmfechaFinal))
        diccioFormulasReporte.Add("cajera", prmCajera)

        ControlEfectivoClass.llamarReporteCentralizado(RutaReportes + "/rptReporteControlEfectivo", DataSet, diccioFormulasReporte) ''MANDAMOS LLAMAR EL REPORTE
        '''''MANDAMOS EL REPORTE (INICIO)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If CDate(Me.dtpFechaInicial.Value.ToShortDateString) > CDate(Me.dtpFechaFinal.Value.ToShortDateString) Then
            MsgBox("La Fecha Inicial no puede ser mayor a la Fecha Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        uspReporteControlEfectivo(Me.cmbCajera.SelectedValue, Me.dtpFechaInicial.Value, Me.dtpFechaFinal.Value)
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub dtpFechaInicial_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaInicial.ValueChanged
        Me.dtpFechaFinal.MinDate = Me.dtpFechaInicial.Value
    End Sub

    Private Sub dtpFechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFinal.ValueChanged
        Me.dtpFechaInicial.MaxDate = Me.dtpFechaFinal.Value
    End Sub
End Class


